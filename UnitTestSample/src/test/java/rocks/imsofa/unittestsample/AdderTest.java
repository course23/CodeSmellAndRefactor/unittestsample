/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.unittestsample;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lendle
 */
public class AdderTest {
    
    public AdderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class Adder.
     */
    @org.junit.Test
    public void testAdd() {
        System.out.println("add");
        int value1 = 0;
        int value2 = 100;
        Adder instance = new Adder();
        int expResult = 100;
        int result = instance.add(value1, value2);
        assertEquals(expResult, result);
    }
    
}
